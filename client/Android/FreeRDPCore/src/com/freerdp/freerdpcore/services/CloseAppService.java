package com.freerdp.freerdpcore.services;

import com.freerdp.freerdpcore.application.GlobalApp;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

public class CloseAppService extends IntentService {

	public CloseAppService() {
		super("CloseAppService");
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Boolean msg = intent.getBooleanExtra("CLOSE", false);
		if(msg)
		{
			if(GlobalApp.openedActivities!=null && GlobalApp.openedActivities.size() > 0)
			{
				for (int i=0; i < GlobalApp.openedActivities.size();i++)
				{
					Activity activity = GlobalApp.openedActivities.get(i);
					if(activity!=null)
					{
						activity.finish();
						activity = null;
					}
						
				}
			}
			System.exit(0);
		}
	}
} 
