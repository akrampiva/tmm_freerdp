package com.freerdp.freerdpcore.presentation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.view.inputmethod.EditorInfo;
import android.view.KeyEvent;

import com.freerdp.freerdpcore.R;
import com.freerdp.freerdpcore.application.GlobalApp;
import com.freerdp.freerdpcore.application.InitExtras;
import com.freerdp.freerdpcore.domain.BookmarkBase;
import com.freerdp.freerdpcore.domain.ConnectionReference;
import com.freerdp.freerdpcore.domain.ManualBookmark;

public class InitActivity extends Activity {
	
	public static InitActivity initactivity;

	private boolean tmm_connect(ManualBookmark initBookMark, EditText pin_edit_text) {
		BookmarkBase tempbookmark = GlobalApp.getManualBookmarkGateway().findByLabel(initBookMark.getLabel());
		String pin = pin_edit_text.getText().toString();
		if(tempbookmark!=null && pin.equals(InitExtras.pin))
		{
			connect(tempbookmark);
			return false;
		}
		else {
			Toast toast = Toast.makeText(InitActivity.this, InitActivity.this.getResources().getString(R.string.invalid_pin_code), Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.TOP, 0, 0);
			toast.show();
			return true;
		}
	}
	
	@SuppressLint("InlinedApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		GlobalApp.openedActivities.add(this);
		initactivity = this;
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			InitExtras.useInitExtras = true;
			//load config
			SharedPreferences rdpConf = this.getSharedPreferences(
					"com.freerdp.afreerdp_setting_preferences", MODE_PRIVATE);
			
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			
			if(rdpConf!=null  && rdpConf.getString("hostname",null)!=null)
			{

				String pin_validation_url = rdpConf.getString("pin_validation_url","");
				if (pin_validation_url != null)
					InitExtras.pin_validation_url = pin_validation_url;
				
				String resolution = rdpConf.getString("resolution", "FITSCREEN");
				if (resolution != null)
				{
					if(resolution.equals("FITSCREEN"))
						InitExtras.resolution = InitExtras.FITSCREEN;
					else if(resolution.equals("AUTOMATIC"))
						InitExtras.resolution = InitExtras.AUTOMATIC;
					else if(resolution.equals("CUSTOM"))
						InitExtras.resolution = InitExtras.CUSTOM;
				}

				String screenWidth = rdpConf.getString("screenWidth", "0");
				InitExtras.screenWidth = Integer.parseInt(screenWidth);

				String screenHeight = rdpConf.getString("screenHeight", "0");
				InitExtras.screenHeight =  Integer.parseInt(screenHeight);
				
				String rotation = rdpConf.getString("screen_orientation", "AUTOMATIC");
				if (rotation.equalsIgnoreCase("PORTRAIT")) {
					InitExtras.screen_orientation = InitExtras.SCREEN_ORIENTATION_PORTRAIT;
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				} else if (rotation.equalsIgnoreCase("LANDSCAPE")) {
					InitExtras.screen_orientation = InitExtras.SCREEN_ORIENTATION_LANDSCAPE;
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
				} else //AUTOMATIC
					InitExtras.screen_orientation = InitExtras.SCREEN_ORIENTATION_AUTOMATIC;
					
			}
			
			String hostname = bundle.getString("hostname");
			if (hostname != null)
				InitExtras.hostname = hostname;

			String domain = bundle.getString("domain");
			if (domain != null)
				InitExtras.domain = domain;
			
			Boolean pin_verification = bundle.getBoolean("pin_verification");
			InitExtras.pin_verification = pin_verification;
			String pin = bundle.getString("pin");
			if(pin!=null)
				InitExtras.pin = pin;
			else
				InitExtras.pin = "";
			String url_get_value = bundle.getString("url_get_value");
			if(url_get_value!=null)
				InitExtras.url_get_value= url_get_value;
			String user = bundle.getString("user");
			if(user!=null)
				InitExtras.user= user;
			else
				InitExtras.user = "";
			String password = bundle.getString("password");
			if(password!=null)
				InitExtras.password= password;
			else
				InitExtras.password = "";
		}
		
		if (InitExtras.useInitExtras) {
			
			setContentView(R.layout.init_layout);
    		final ManualBookmark initBookMark = InitExtras.getInitBookMark();
    		
        	if(initBookMark!=null)
        	{
            	BookmarkBase tempbookmark = GlobalApp.getManualBookmarkGateway().findByLabel(initBookMark.getLabel());
        		if(tempbookmark!=null)
        		{
        			GlobalApp.getManualBookmarkGateway().delete(tempbookmark.getId());
        		}
        		GlobalApp.getManualBookmarkGateway().insert(initBookMark);
        		tempbookmark = GlobalApp.getManualBookmarkGateway().findByLabel(initBookMark.getLabel());
        		if(tempbookmark!=null)
            	{
            		initBookMark.setId(tempbookmark.getId());
            	}
        	}

       	
        	//
        	
        	TextView.OnEditorActionListener pinEditTextListener = new TextView.OnEditorActionListener(){
        		public boolean onEditorAction(TextView pin_edit_text, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_DONE) { 
						return tmm_connect(initBookMark, (EditText) pin_edit_text);
					}
					return true;
        		}
        	};

      	
        	final EditText pin_edit_text=(EditText)findViewById(R.id.pin_edit_text);
        	TextView pin_text_view=(TextView)findViewById(R.id.pin_text_view);
        	pin_edit_text.setOnEditorActionListener(pinEditTextListener);
        	  	
        	Button connectBtn = (Button)findViewById(R.id.connect_btn);
        	connectBtn.setOnClickListener(new Button.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					tmm_connect(initBookMark, pin_edit_text);
				}
			});
        	
        	if(InitExtras.pin_verification && InitExtras.pin.length() > 0)
        	{
        		connectBtn.setVisibility(View.VISIBLE);
        		pin_edit_text.setVisibility(View.VISIBLE);
        	}
        	else
        	{
            	BookmarkBase tempbookmark = GlobalApp.getManualBookmarkGateway().findByLabel(initBookMark.getLabel());
        		pin_text_view.setText(this.getResources().getString(R.string.connect_to_the_server)+"...");
        		connect(tempbookmark);
        	}
 
    	}
		else
		{
			
			Intent homeIntent = new Intent(this, HomeActivity.class);
    		startActivity(homeIntent);
		}
	}
	
	public void connect(BookmarkBase bookmark)
	{
		Bundle bundle = new Bundle();
		String refStr = ConnectionReference.getManualBookmarkReference(bookmark.getId());
		bundle.putString(SessionActivity.PARAM_CONNECTION_REFERENCE, refStr );

		Intent sessionIntent = new Intent(InitActivity.this, SessionActivity.class);
		sessionIntent.putExtras(bundle);
		startActivity(sessionIntent);
		if(InitExtras.useInitExtras && !InitExtras.pin_verification)
		{
			finish();
		}
	}
	
	public static void finishApp() {
		// TODO Auto-generated method stub
		if(initactivity!=null)
			initactivity.finish();
	}
	
}
