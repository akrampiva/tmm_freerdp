/*
   Android Touch Pointer view

   Copyright 2013 Thincast Technologies GmbH, Author: Martin Fleisz

   This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0. 
   If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/

package com.freerdp.freerdpcore.presentation;

import com.freerdp.freerdpcore.R;
import com.freerdp.freerdpcore.utils.GestureDetector;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;

public class TmmTouchPointerView extends ImageView {

	// touch pointer listener - is triggered if an action field is 
	public interface TouchPointerListener {
		abstract void onTouchPointerMove(int x, int y);
		abstract void onTouchPointerToggleKeyboard();
		abstract void onTouchPointerToggleExtKeyboard();
	}
	
	
	private class UIHandler extends Handler {
				
		UIHandler() {
			super();
		}
		
		@Override
		public void handleMessage(Message msg) {
			setPointerImage(R.drawable.tmm_touch_pointer_default);
		}
	}	
	
	
	
	// the touch pointer consists of 9 quadrants with the following functionality:
	//
	// ---------
	// | 0 | 1 | 
	// ---------
	//
	// 0 ... quadrant that issue a callback to the keyboard
	// 1 ... quadrant that issue a callback to the extended keyboard
	// 0, 1 (long press) ... used to drag the pointer
		

	private static final int POINTER_ACTION_KEYBOARD = 0;	
	private static final int POINTER_ACTION_EXTKEYBOARD = 1;	

	private static final int DEFAULT_TOUCH_POINTER_RESTORE_DELAY = 150; 
	
	private RectF pointerRect;
	private RectF pointerAreaRects[] = new RectF[2];
	private Matrix translationMatrix;
	private boolean pointerMoving = false;
	private TouchPointerListener listener = null;
	private UIHandler uiHandler = new UIHandler();
	
	// gesture detection
	private GestureDetector gestureDetector;

	private class TouchPointerGestureListener extends GestureDetector.SimpleOnGestureListener {
		
		private MotionEvent prevEvent = null;
		
		public boolean onDown(MotionEvent e) {
			prevEvent = MotionEvent.obtain(e);
			return true;
		}
		
		public boolean onUp(MotionEvent e) {
			if(prevEvent != null)
			{
				prevEvent.recycle();
				prevEvent = null;
			}
			
			pointerMoving = false;
			return true;
		}
		
		public void onLongPress(MotionEvent e) {
			prevEvent = MotionEvent.obtain(e);
			pointerMoving = true;
			setPointerImage(R.drawable.tmm_touch_pointer_moving);
       }

        public void onLongPressUp(MotionEvent e) {
        	if(pointerMoving)
        	{
				setPointerImage(R.drawable.tmm_touch_pointer_default);				
				pointerMoving = false;
        	}
        }
        
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
			// move pointer graphics
        	if(pointerMoving)
        	{
        		// move pointer graphics
				int e2_x = (int)e2.getX();
				int e2_y = (int)e2.getY();
				float[] pointerPos = getPointerPosition();
				float next_x = e2.getX() - prevEvent.getX() + pointerPos[0] + getPointerWidth() ;
				float next_y = e2.getY() - prevEvent.getY() + pointerPos[1] + getPointerHeight();

				if ( (next_x < getPointerWidth()) || (next_x > getMeasuredWidth()) )
				{
					e2_x = (int)prevEvent.getX();
				}
				if ((next_y < getPointerHeight()) || (next_y > getMeasuredHeight()) )
				{
					e2_y = (int)prevEvent.getY();
				}
				e2.setLocation(e2_x, e2_y);
				movePointer((int)(e2_x - prevEvent.getX()), (int)(e2_y - prevEvent.getY()));
        		/*movePointer((int)(e2.getX() - prevEvent.getX()), (int)(e2.getY() - prevEvent.getY()));*/
	        	prevEvent.recycle();
	        	prevEvent = MotionEvent.obtain(e2);				
	
	        	// send move notification
	        	RectF rect = getCurrentPointerArea(POINTER_ACTION_KEYBOARD);
	        	listener.onTouchPointerMove((int)rect.centerX(), (int)rect.centerY());
        	}
            return true;
		}

        public boolean onSingleTapUp(MotionEvent e) {
        	// look what area got touched and fire actions accordingly
        	if(pointerAreaTouched(e, POINTER_ACTION_KEYBOARD))
           	{
        		displayPointerImageAction(R.drawable.tmm_touch_pointer_keyboard);           		
           		listener.onTouchPointerToggleKeyboard();
           	}
           	else if(pointerAreaTouched(e, POINTER_ACTION_EXTKEYBOARD))
           	{
           		displayPointerImageAction(R.drawable.tmm_touch_pointer_extkeyboard);
           		listener.onTouchPointerToggleExtKeyboard();
           	}
           	return true;
        }		
        
	}	

	public TmmTouchPointerView(Context context) {
		super(context);
		initTouchPointer(context);
	}

	public TmmTouchPointerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initTouchPointer(context);
	}

	public TmmTouchPointerView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initTouchPointer(context);
	}

	private void initTouchPointer(Context context) {
		gestureDetector = new GestureDetector(context, new TouchPointerGestureListener(), null, true);
		gestureDetector.setLongPressTimeout(500);
		translationMatrix = new Matrix();
		setScaleType(ScaleType.MATRIX);
		setImageMatrix(translationMatrix);
				
		// init rects
		final float rectSizeWidth = (float)getDrawable().getIntrinsicWidth() / 2.0f;
		final float rectSizeHeight = (float)getDrawable().getIntrinsicHeight();
		for(int i = 0; i < 2; i++)
		{
			int left = (int)(i * rectSizeWidth);
			int top = 0;
			int right = left + (int)rectSizeWidth;
			int bottom = (int)rectSizeHeight;
			pointerAreaRects[i] = new RectF(left, top, right, bottom); 
		}
		pointerRect = new RectF(0, 0, getDrawable().getIntrinsicWidth(), getDrawable().getIntrinsicHeight());
	}

	public void setTouchPointerListener(TouchPointerListener listener) {
		this.listener = listener;
	}
	
	public int getPointerWidth() {
		return getDrawable().getIntrinsicWidth();
	}

	public int getPointerHeight() {
		return getDrawable().getIntrinsicHeight();
	}	
	
	public float[] getPointerPosition() {
		float []curPos = new float[2];
		translationMatrix.mapPoints(curPos);
		return curPos;
	}
	
	private void movePointer(float deltaX, float deltaY) {
		translationMatrix.postTranslate(deltaX, deltaY);
		setImageMatrix(translationMatrix);
	}
	
	public void setInitialPosition(int screen_width, int screen_height) {
		int ix, iy;
		ix = screen_width - getDrawable().getIntrinsicWidth() - 250;
		iy = screen_height - getDrawable().getIntrinsicHeight();
		movePointer(ix, iy);
	}
	
	private void ensureVisibility(int screen_width, int screen_height)
	{
		float []curPos = new float[2];
		translationMatrix.mapPoints(curPos);
		
	    if (curPos[0] > (screen_width - pointerRect.width()))
	    	curPos[0] = screen_width - pointerRect.width();
	    if (curPos[0] < 0)
	    	curPos[0] = 0;
	    if (curPos[1] > (screen_height - pointerRect.height()))
	    	curPos[1] = screen_height - pointerRect.height();
	    if (curPos[1] < 0)
	    	curPos[1] = 0;

	    translationMatrix.setTranslate(curPos[0], curPos[1]);
	    setImageMatrix(translationMatrix);	
	}
	
	private void displayPointerImageAction(int resId)
	{
		setPointerImage(resId);
		uiHandler.sendEmptyMessageDelayed(0, DEFAULT_TOUCH_POINTER_RESTORE_DELAY);
	}
	
	private void setPointerImage(int resId)
	{
		setImageResource(resId);
	}
	
	// returns the pointer area with the current translation matrix applied
	private RectF getCurrentPointerArea(int area) {
		RectF transRect = new RectF(pointerAreaRects[area]);
		translationMatrix.mapRect(transRect);
		return transRect;						
	}

	private boolean pointerAreaTouched(MotionEvent event, int area) {				
		RectF transRect = new RectF(pointerAreaRects[area]);
		translationMatrix.mapRect(transRect);
		if(transRect.contains(event.getX(), event.getY()))
			return true;			
		return false;
	}	
	
	private boolean pointerTouched(MotionEvent event) {				
		RectF transRect = new RectF(pointerRect);
		translationMatrix.mapRect(transRect);
		if(transRect.contains(event.getX(), event.getY()))
			return true;			
		return false;
	}			

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// check if pointer is being moved or if we are in scroll mode or if the pointer is touched
		if(!pointerMoving && !pointerTouched(event))
			return false;
		return gestureDetector.onTouchEvent(event);
	}		

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		// ensure touch pointer is visible
		if (changed)
			ensureVisibility(right - left, bottom - top);		
	}
}
