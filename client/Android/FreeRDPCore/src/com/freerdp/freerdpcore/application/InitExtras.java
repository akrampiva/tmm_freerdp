package com.freerdp.freerdpcore.application;

import com.freerdp.freerdpcore.domain.BookmarkBase.ScreenSettings;
import com.freerdp.freerdpcore.domain.ManualBookmark;

public class InitExtras {
	
	//constants
	public static final String TMM_DEFAULT_ACCOUNT = "TMM_DEFAULT_ACCOUNT_52487954";
	public static final int FITSCREEN = -2;
	public static final int AUTOMATIC = -1;
	public static final int CUSTOM = 0;
	public static final int SCREEN_ORIENTATION_PORTRAIT = -2;
	public static final int SCREEN_ORIENTATION_LANDSCAPE = -1;
	public static final int SCREEN_ORIENTATION_AUTOMATIC = 0;

	//
	
	//make it tree to activate manual freerdp configuration
	public static Boolean useInitExtras = false;
	//pre-configured fields
	public static String hostname = "";
	public static String domain = "";
	public static String pin_validation_url;
	public static int resolution = FITSCREEN;
	public static int screenWidth = 0;
	public static int screenHeight = 0;
	public static int screen_orientation = SCREEN_ORIENTATION_AUTOMATIC;
	//other fields
	public static String user = "";
	public static String password = "";
	public static Boolean pin_verification = false;
	public static String pin;
	public static String url_get_value;
	
	//
	public static ManualBookmark initBookmark;
	public static ManualBookmark getInitBookMark(){
		initBookmark =  new ManualBookmark();
		initBookmark.setDomain(domain);
		initBookmark.setLabel(TMM_DEFAULT_ACCOUNT);
		initBookmark.setHostname(hostname);
		
		initBookmark.setUsername(user);
		initBookmark.setPassword(password);
		
		if(screenHeight > 0 && screenWidth > 0)
		{
			ScreenSettings screenSettings =  new ScreenSettings();
			screenSettings.setWidth(screenWidth);
			screenSettings.setHeight(screenHeight);
			screenSettings.setResolution(resolution);
			initBookmark.setScreenSettings(screenSettings);
		}
		return initBookmark;
	}
	
	public static long getBookMarkId(){
		return initBookmark.getId();
	}
}
